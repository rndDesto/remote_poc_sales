import {useState} from 'react'
type propsHeading ={
    title:string
}

const Heading = (props:propsHeading) => {
    const {title="kosong"}=props
    const [gantiTitle,setGantiTitle] = useState<string>('nanti akan merubah')
  return (
    <div>
        <p>{title}</p>
        <button onClick={()=>setGantiTitle('text ny berubah gan')}>ganti </button>
        <p>nanti yang ini berubah {gantiTitle}</p>
    </div>
  )
}

export default Heading