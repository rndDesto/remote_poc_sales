import styles from './Brand.module.scss'

const Brand = () => {
  return (
    <div className={styles.root}>
        <h1>Brand Page</h1>
        <button onClick={()=>window.localStorage.clear()}>Clear Storage host</button>
    </div>
  )
}

export default Brand