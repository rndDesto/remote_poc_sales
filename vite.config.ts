import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'
import federation from "@originjs/vite-plugin-federation";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    federation({
      name: "remote_sales",
      filename: "remoteEntry.js",
      exposes: {
        "./BrandPage": "./src/Pages/Brand",
        "./PartnerPage": "./src/Pages/Partner",
      },
      shared: ["react", "react-dom"],
    })
  ],
  server: {
    port:4102,
  },
  preview: {
    port: 5002,
  },
  build: {
    modulePreload: false,
    target: "esnext",
    minify: false,
    cssCodeSplit: false,
  }
})
